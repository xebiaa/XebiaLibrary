package library

class IssueBookController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [issueBookInstanceList: IssueBook.list(params), issueBookInstanceTotal: IssueBook.count()]
    }

    def create = {
        def issueBookInstance = new IssueBook()
        issueBookInstance.properties = params
        return [issueBookInstance: issueBookInstance]
    }

    def save = {
        def issueBookInstance = new IssueBook(params)
        if (issueBookInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'issueBook.label', default: 'IssueBook'), issueBookInstance.id])}"
            redirect(action: "show", id: issueBookInstance.id)
        }
        else {
            render(view: "create", model: [issueBookInstance: issueBookInstance])
        }
    }

    def show = {
        def issueBookInstance = IssueBook.get(params.id)
        if (!issueBookInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'issueBook.label', default: 'IssueBook'), params.id])}"
            redirect(action: "list")
        }
        else {
            [issueBookInstance: issueBookInstance]
        }
    }

    def edit = {
        def issueBookInstance = IssueBook.get(params.id)
        if (!issueBookInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'issueBook.label', default: 'IssueBook'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [issueBookInstance: issueBookInstance]
        }
    }

    def update = {
        def issueBookInstance = IssueBook.get(params.id)
        if (issueBookInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (issueBookInstance.version > version) {

                    issueBookInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'issueBook.label', default: 'IssueBook')] as Object[], "Another user has updated this IssueBook while you were editing")
                    render(view: "edit", model: [issueBookInstance: issueBookInstance])
                    return
                }
            }
            issueBookInstance.properties = params
            if (!issueBookInstance.hasErrors() && issueBookInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'issueBook.label', default: 'IssueBook'), issueBookInstance.id])}"
                redirect(action: "show", id: issueBookInstance.id)
            }
            else {
                render(view: "edit", model: [issueBookInstance: issueBookInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'issueBook.label', default: 'IssueBook'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def issueBookInstance = IssueBook.get(params.id)
        if (issueBookInstance) {
            try {
                issueBookInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'issueBook.label', default: 'IssueBook'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'issueBook.label', default: 'IssueBook'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'issueBook.label', default: 'IssueBook'), params.id])}"
            redirect(action: "list")
        }
    }
}
