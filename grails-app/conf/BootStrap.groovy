import library.User

class BootStrap {

    def init = { servletContext ->
        final String BACKUP_ADMIN = 'adminSohil'
        if (!User.findByUserId(BACKUP_ADMIN)) {
            new User(userId:BACKUP_ADMIN,password:'password').save()
        }
    }
        def destroy = {
        }
    }
