package library

class IssueBook {
    Date issueDate
    Date dateOfReturn

    static belongsTo = [user:User , book:Book]

    static constraints = {
    }
}
