package library

class Book {

    String name;
    String author;
    String ISBN;
    String Edition;
    Date inHouse;
    Date purchased;
    String comments;
    Integer quantity =1;

    static hasMany = [issueBook:IssueBook]

    static constraints = {
        name(blank: false)
        author(blank: false)
        ISBN(blank:false)
        Edition(blank: false)
        purchased(blank: false)
        comments(blank: true)
        inHouse(blank: true)
    }
}
