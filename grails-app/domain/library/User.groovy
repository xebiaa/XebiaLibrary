package library

class User {
    String displayName
    String userId
    String password

    static hasMany = [issueBook:IssueBook];

    static constraints = {
        userId(length:6..8,unique:true)
        password(length:6..8)
    }


}
