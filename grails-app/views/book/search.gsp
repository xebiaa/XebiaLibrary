<%--
  Created by IntelliJ IDEA.
  User: Sohil
  Date: 10/3/11
  Time: 1:45 AM
  To change this template use File | Settings | File Templates.
--%>

<%@ page import="library.Book" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'book.label', default: 'Book')}"/>
    <title><g:message code="default.search.label" args="[entityName]"/></title>
</head>

<body>
<div class="nav">
    <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a>
    </span>
    <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label"
                                                                           args="[entityName]"/></g:link></span>
    <span class="menuButton"><g:link class="create" action="create"><g:message code="default.create.label"
                                                                               args="[entityName]"/></g:link></span>
</div>

<div class="body">
    <h1><g:message code="default.search.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${bookInstance}">
        <div class="errors">
            <g:renderErrors bean="${bookInstance}" as="list"/>
        </div>
    </g:hasErrors>
    <g:form action="search">
        <div class="dialog">
            <table>
                <tbody>

                <tr class="prop">
                    <td valign="top" class="name">
                        <label for="name"><g:message code="book.name.label" default="Name"/></label>
                    </td>
                    <td valign="top" class="value ${hasErrors(bean: bookInstance, field: 'name', 'errors')}">
                        <g:textField name="name" value="${bookInstance?.name}"/>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

       <div class="buttons">
        <input type="submit" value="Search"
        class="formbutton">
        </input>
        </div>
    </g:form>
</div>
</body>
</html>
