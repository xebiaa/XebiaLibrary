
<%@ page import="library.IssueBook" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'issueBook.label', default: 'IssueBook')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'issueBook.id.label', default: 'Id')}" />
                        
                            <th><g:message code="issueBook.book.label" default="Book" /></th>
                        
                            <g:sortableColumn property="dateOfReturn" title="${message(code: 'issueBook.dateOfReturn.label', default: 'Date Of Return')}" />
                        
                            <g:sortableColumn property="issueDate" title="${message(code: 'issueBook.issueDate.label', default: 'Issue Date')}" />
                        
                            <th><g:message code="issueBook.user.label" default="User" /></th>
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${issueBookInstanceList}" status="i" var="issueBookInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${issueBookInstance.id}">${fieldValue(bean: issueBookInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: issueBookInstance, field: "book")}</td>
                        
                            <td><g:formatDate date="${issueBookInstance.dateOfReturn}" /></td>
                        
                            <td><g:formatDate date="${issueBookInstance.issueDate}" /></td>
                        
                            <td>${fieldValue(bean: issueBookInstance, field: "user")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${issueBookInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
