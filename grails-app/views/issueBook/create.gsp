

<%@ page import="library.IssueBook" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'issueBook.label', default: 'IssueBook')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${issueBookInstance}">
            <div class="errors">
                <g:renderErrors bean="${issueBookInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="book"><g:message code="issueBook.book.label" default="Book" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: issueBookInstance, field: 'book', 'errors')}">
                                    <g:select name="book.id" from="${library.Book.list()}" optionKey="id" value="${issueBookInstance?.book?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateOfReturn"><g:message code="issueBook.dateOfReturn.label" default="Date Of Return" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: issueBookInstance, field: 'dateOfReturn', 'errors')}">
                                    <g:datePicker name="dateOfReturn" precision="day" value="${issueBookInstance?.dateOfReturn}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="issueDate"><g:message code="issueBook.issueDate.label" default="Issue Date" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: issueBookInstance, field: 'issueDate', 'errors')}">
                                    <g:datePicker name="issueDate" precision="day" value="${issueBookInstance?.issueDate}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="user"><g:message code="issueBook.user.label" default="User" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: issueBookInstance, field: 'user', 'errors')}">
                                    <g:select name="user.id" from="${library.User.list()}" optionKey="id" value="${issueBookInstance?.user?.id}"  />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
